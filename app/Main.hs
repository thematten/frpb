module Main (main) where

import Data.Bifunctor
import Data.Bool
import Data.Functor
import Data.Maybe
import Data.Text (Text)
import Data.Text.IO qualified as T
import FRP.Expression
import FRP.Parser
import Numeric.Natural
import System.IO
import Text.Megaparsec
import Text.Megaparsec.Char qualified as ML

main :: IO ()
main = hSetBuffering stdout NoBuffering *> whileM do
  T.putStr "FRP> "
  parseCommand <$> T.getLine >>= \case
    Left  msg -> do
      putStrLn msg
      T.putStrLn "use \":help\" to see available syntax"
      continue
    Right cmd -> case cmd of
      Help -> T.putStrLn help *> continue
      Exit -> exit
      Root var start end (fromMaybe 100 -> i) expr -> do
        case fun var expr of
          Left  x -> unboundError x
          Right f -> case root f (start, end) i of
            Nothing -> T.putStrLn "no root found (try different range)"
            Just r  -> print r
        continue
      Integral var start end (fromMaybe 100 -> points) expr -> do
        case fun var expr of
          Left  x -> unboundError x
          Right f -> print $ integral f (start, end) points
        continue
      Expression expr -> do
        either unboundError print $ evaluate expr
        continue
 where
  help :: Text = "Commands:\n\
    \  (expression) - evaluate expression\n\
    \  :help - this info\n\
    \  :exit - exit REPL\n\
    \  :root(var, start, end [, iterations]) expression - approximate root\n\
    \  :integral(var, start, end [, points]) expression - approximate integral\n"
  unboundError x =
    putStrLn $ "error: function contains unbound variable " ++ show x

-------------------------------------------------------------------------------
whileM :: Monad m => m Bool -> m ()
whileM fa = fa >>= bool (pure ()) (whileM fa)

continue, exit :: Applicative f => f Bool
continue = pure True
exit     = pure False

-------------------------------------------------------------------------------
data Command
  = Help
  | Exit
  | Root Char Double Double (Maybe Natural) Expression
  | Integral Char Double Double (Maybe Natural) Expression
  | Expression Expression

-------------------------------------------------------------------------------
parseCommand :: Text -> Either String Command
parseCommand = first errorBundlePretty . parse (command <* eof) "input"

-------------------------------------------------------------------------------
command, helpCommand, exitCommand, rootCommand, integralCommand
  , expressionCommand :: Parser Command

command = choice
  [ helpCommand
  , exitCommand
  , rootCommand
  , integralCommand
  , expressionCommand
  ]

helpCommand = symbol ":help" *> spacedRest $> Help

exitCommand = symbol ":exit" *> spacedRest $> Exit

rootCommand = do
  _ <- symbol ":root"
  parens (Root
      <$>                          ML.letterChar
      <*            symbol "," <*> numberExpression
      <*            symbol "," <*> numberExpression
      <*> optional (symbol ","  *> decimalExpression)
    ) <*>                          expression

integralCommand = do
  _ <- symbol ":integral"
  parens (Integral
      <$>                          ML.letterChar
      <*            symbol "," <*> numberExpression
      <*            symbol "," <*> numberExpression
      <*> optional (symbol ","  *> decimalExpression)
    ) <*> expression

expressionCommand = Expression <$> expression
