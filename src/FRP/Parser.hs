module FRP.Parser
  ( Parser, space, lexeme, symbol, binary, prefix, parens, spacedRest, floating
  ) where

import Control.Monad.Combinators
import Control.Monad.Combinators.Expr
import Data.Functor
import Data.Scientific (toRealFloat)
import Data.Text (Text)
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char qualified as M
import Text.Megaparsec.Char.Lexer qualified as ML

-------------------------------------------------------------------------------
type Parser = Parsec Void Text

space :: Parser ()
space = ML.space M.space1 empty empty

lexeme :: Parser a -> Parser a
lexeme = ML.lexeme space

symbol :: Text -> Parser Text
symbol = ML.symbol space

binary :: (Parser (a -> a -> a) -> Operator Parser a)
       -> Text
       -> (a -> a -> a)
       -> Operator Parser a
binary con name op = con $ op <$ symbol name

prefix :: Text -> (a -> a) -> Operator Parser a
prefix name op = Prefix $ op <$ symbol name

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

spacedRest :: Parser Text
spacedRest = (eof $> "") <|> (space *> takeRest)

floating :: RealFloat a => Parser a
floating = toRealFloat <$> ML.scientific
