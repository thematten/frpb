module FRP.Expression
  ( Expression (..), parseExpression, expression
  , numberExpression, decimalExpression
  , normalize, evaluate
  , Function, fun
  , root, integral
  ) where

import Control.Monad
import Control.Monad.Combinators
import Control.Monad.Combinators.Expr
import Data.Bifunctor
import Data.Function
import Data.Functor
import Data.Functor.Compose
import Data.Functor.Identity
import Data.Text (Text)
import FRP.Parser
import GHC.Float
import Numeric.Natural
import Text.Megaparsec
import Text.Megaparsec.Char qualified as M

-------------------------------------------------------------------------------
data Expression
  = Pi
  | Euler
  | Num Double
  | Var Char
  | Abs Expression
  | Signum Expression
  | Sin Expression
  | Cos Expression
  | Expression :+: Expression
  | Expression :*: Expression
  | Expression :^: Expression
  deriving stock Show

infixl 6 :+:
infixl 7 :*:
infixr 8 :^:

instance Num Expression where
  (+)         = (:+:)
  (*)         = (:*:)
  abs         = Abs
  signum      = Signum
  fromInteger = Num . fromInteger

  x - y  = x + (-y)
  negate = (-1 *)

instance Fractional Expression where
  fromRational = Num . fromRational
  x / y = x * y :^: -1

parseExpression :: Text -> Either String Expression
parseExpression = first errorBundlePretty . parse (expression <* eof) "input"

-------------------------------------------------------------------------------
expression, term, number, piNumber, eulerNumber, var, absolute
  :: Parser Expression

expression  = label "expression" $ makeExprParser term
  [ [ prefix "-"      negate
    , prefix "+"      id
    , prefix "signum" signum
    , prefix "sin"    Sin
    , prefix "cos"    Cos
    ]
  , [ binary InfixR "^" (:^:)
    ]
  , [ binary InfixL "*" (*)
    , binary InfixL "/" (/)
    ]
  , [ binary InfixL "+" (+)
    , binary InfixL "-" (-)
    ]
  ]

term        = choice
  [ number
  , piNumber
  , eulerNumber
  , var
  , absolute
  , parens expression
  ]

number      = lexeme $ label "number" $ Num <$> floating

piNumber    = choice (symbol <$> ["pi", "π"]) $> Pi

eulerNumber = symbol "e" $> Euler

var         = lexeme $ label "variable" $ Var <$> M.letterChar

absolute    = label "absolute value" $
  between (symbol "|") (symbol "|") $ Abs <$> expression

-------------------------------------------------------------------------------
numberExpression :: Parser Double
numberExpression = evaluate <$> expression >>= \case
  Left  x -> fail $ "expression contains unbound variable " ++ show x
  Right n -> pure n

decimalExpression :: Parser Natural
decimalExpression = numberExpression >>= \case
  n | n >= 0 -> pure $ truncate n
  _ -> fail "negative literal, expected natural number"


-------------------------------------------------------------------------------
normalizeA :: Applicative m
           => (Char -> m (Either Expression Double))
           -> Expression -> m (Either Expression Double)
normalizeA f = fix \go -> \case
  Pi       -> pure $ Right pi
  Euler    -> pure $ Right $ exp 1
  Num n    -> pure $ Right n
  Var x    -> f x
  Abs e    -> bimap Abs    abs    <$> go e
  Signum e -> bimap Signum signum <$> go e
  Sin e    -> bimap Sin    sin    <$> go e
  Cos e    -> bimap Cos    cos    <$> go e
  x :+: y  -> bibimap (:+:) (+)         Num <$> go x <*> go y
  x :*: y  -> bibimap (:*:) (*)         Num <$> go x <*> go y
  x :^: y  -> bibimap (:^:) powerDouble Num <$> go x <*> go y
 where
  bibimap :: (a -> a -> a)
          -> (b -> b -> b)
          -> (b -> a)
          -> Either a b -> Either a b -> Either a b
  bibimap onL onR rToL x y
    | Right x' <- x
    , Right y' <- y = Right $ x' `onR` y'
    | otherwise     = Left  $ (onL `on` either id rToL) x y

normalize :: Expression -> Expression
normalize = either id Num . runIdentity . normalizeA (pure . Left . Var)

evaluate :: Expression -> Either Char Double
evaluate = normalizeA Left >=> either undefined Right

-------------------------------------------------------------------------------
type Function = Double -> Double

fun :: Char -> Expression -> Either Char Function
fun x =
  (fmap . fmap) (either undefined id) . getCompose . normalizeA go
 where
  go y = Compose if x == y then Right Right else Left y

-------------------------------------------------------------------------------
root :: Function -> (Double, Double) -> Natural -> Maybe Double
root f = uncurry go where
  go start end i
    | i == 0 || ms == 0 = Just middle
    | ss      == 0      = Just start
    | es      == 0      = Just end
    | ss * ms == -1     = go start middle $ i - 1
    | ms * es == -1     = go middle   end $ i - 1
    | otherwise         = Nothing
   where
    middle = (start + end) / 2
    ss = signum $ f start
    ms = signum $ f middle
    es = signum $ f end

-------------------------------------------------------------------------------
integral :: Function -> (Double, Double) -> Natural -> Double
integral f (start, end) points =
  go $ [0..parts] <&> \p -> start + fromIntegral p * part
 where
  part  = (end - start) / fromIntegral parts
  parts = points + 1

  go (x1:x2:xs) = (x2 - x1) * f (x1 + part / 2) + go (x2:xs)
  go _          = 0
